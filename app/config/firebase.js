import firebase from 'firebase';

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyCgmB_jF-8zBjJmf7v3OBXuP-si-Lzr19g',
  authDomain: 'fides-32f17.firebaseapp.com',
  databaseURL: 'https://fides-32f17.firebaseio.com',
  projectId: 'fides-32f17',
  storageBucket: 'fides-32f17.appspot.com',
  messagingSenderId: '394898389561',
  appId: '1:394898389561:web:30d785c360c39dea'
};
  // Initialize Firebase
const confiFirebase = firebase.initializeApp(firebaseConfig);
export default confiFirebase;
