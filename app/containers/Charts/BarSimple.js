import React from 'react';
import PropTypes from 'prop-types';
import { createMuiTheme, withStyles } from '@material-ui/core/styles';
import ThemePallete from 'dan-api/palette/themePalette';
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  CartesianAxis,
  Tooltip,
  Legend,
  ResponsiveContainer
} from 'recharts';
import confiFirebase from '../../config/firebase';
import styles from './fluidChart-jss';

const theme = createMuiTheme(ThemePallete.magentaTheme);
const color = ({
  primary: theme.palette.primary.main,
  primaryDark: theme.palette.primary.dark,
  secondary: theme.palette.secondary.main,
  secondaryDark: theme.palette.secondary.dark,
});

class BarSimple extends React.Component {
  state = {
    dataRe: []
  };

  componentWillMount = () => {
    const reportRef = confiFirebase.database().ref('reports');
    reportRef.on('value', snapshot => {
      let data = [];
      snapshot.forEach(childSnapshot => {
        data = data.concat(childSnapshot.val());
      });
      this.setState({
        dataRe: data
      });
    });
  }

  render() {
    const { classes } = this.props;
    const { dataRe } = this.state;
    return (
      <div className={classes.chartFluid}>
        <ResponsiveContainer>
          <BarChart
            width={800}
            height={450}
            data={dataRe}
            margin={{
              top: 5,
              right: 30,
              left: 20,
              bottom: 5
            }}
          >
            <defs>
              <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                <stop offset="5%" stopColor={color.primary} stopOpacity={0.8} />
                <stop offset="95%" stopColor={color.primaryDark} stopOpacity={1} />
              </linearGradient>
              <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
                <stop offset="5%" stopColor={color.secondary} stopOpacity={0.8} />
                <stop offset="95%" stopColor={color.secondaryDark} stopOpacity={1} />
              </linearGradient>
            </defs>
            <XAxis dataKey="name" tickLine={false} />
            <YAxis axisLine={false} tickSize={3} tickLine={false} tick={{ stroke: 'none' }} />
            <CartesianGrid vertical={false} strokeDasharray="3 3" />
            <CartesianAxis vertical={false} />
            <Tooltip />
            <Legend />
            <Bar dataKey="boys" name="Niños" fillOpacity="0.6" fill="blue" />
            <Bar dataKey="men" name="Hombres" fillOpacity="0.6" fill="green" />
            <Bar dataKey="women" name="Mujeres" fillOpacity="0.5" fill="pink" />
          </BarChart>
        </ResponsiveContainer>
      </div>
    );
  }
}

BarSimple.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(BarSimple);
