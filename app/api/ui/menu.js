module.exports = [
  {
    key: 'profile',
    name: 'Perfil',
    icon: 'ios-contact',
    link: '/app/profile'
  },
  {
    key: 'chart',
    name: 'Estadísticas',
    icon: 'ios-pie-outline',
    link: '/app/sitios'
  },
  {
    key: 'map',
    name: 'Ubicación',
    icon: 'ios-pin',
    link: '/app/maps'
  }
];
