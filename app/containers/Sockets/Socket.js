import React from 'react';
import { Helmet } from 'react-helmet';
import brand from 'dan-api/dummy/brand';
import { PapperBlock } from 'dan-components';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import ArrowForward from '@material-ui/icons/ArrowForward';
/**
 * Form Formik
 */
import { Formik, Form, Field } from 'formik';
import { TextField } from 'formik-material-ui';
import confiFirebase from '../../config/firebase';

class Socket extends React.Component {
  state = {
    report: 0,
    user: 0
  }

  handleForm = (values) => {
    let gender = values.gender == 1 ? 'men' : (values.gender == 2 ? 'women' : (values.gender == 3 ? 'boys' : 'boys'));

    confiFirebase.database().ref(`reports/${values.site}/${gender}`).once('value', (data) => {
      this.state.report = data.val();
      let totalReport = values.gender == 1 ? { men: this.state.report + 1 } : (values.gender == 2 ? { women: this.state.report + 1 } : (values.gender == 3 ? { boys: this.state.report + 1 } : { boys: this.state.report + 1 }));
      confiFirebase.database().ref(`reports/${values.site}`).update(totalReport);
      if (values.gender == 1) {
        confiFirebase.database().ref(`users/${values.site}/value`).once('value', (dataUser) => {
          this.state.user = dataUser.val();
          confiFirebase.database().ref(`users/${values.site}`).update(
            { value: this.state.user + 1 }
          );
        });
      }
    });
    /* 1.Restaurantes
    2.Piscinas
    3.Boutique
    4.Bar */
    /*
    1. hombre
    2. mujer
    */
  }

  render() {
    const { pristine, submitting } = this.props;
    const title = brand.name + ' - Socket';
    const description = brand.desc;
    return (
      <div>
        <Helmet>
          <title>{title}</title>
          <meta name="description" content={description} />
          <meta property="og:title" content={title} />
          <meta property="og:description" content={description} />
          <meta property="twitter:title" content={title} />
          <meta property="twitter:description" content={description} />
        </Helmet>
        <PapperBlock title="Sockets" icon="ios-stats-outline" desc="" overflowX>
          <div>
            <section>
              <Formik
                onSubmit={(values, actions) => {
                  // same shape as initial values
                  actions.setSubmitting(false);
                  this.handleForm(values);
                }}
              >
                {() => (
                  <Form>
                    <div>
                      <FormControl>
                        <Field
                          name="gender"
                          type="text"
                          component={TextField}
                          placeholder="Genero"
                          label="Tipo de persona"
                        />
                      </FormControl>
                    </div>
                    <div>
                      <FormControl>
                        <Field
                          name="site"
                          type="text"
                          component={TextField}
                          placeholder="Sitio"
                          label="Tipo de sitio"
                        />
                      </FormControl>
                    </div>
                    <div />
                    <div>
                      <FormControl>
                        <Button
                          variant="contained"
                          fullWidth
                          color="primary"
                          size="large"
                          type="submit"
                        >
                    Continuar
                          <ArrowForward
                            disabled={submitting || pristine}
                          />
                        </Button>
                      </FormControl>
                    </div>
                  </Form>
                )}
              </Formik>
            </section>
          </div>
        </PapperBlock>
      </div>
    );
  }
}

export default Socket;
